//Settiamo le varibili prima dell'esecuzione altrimenti prenderanno i valori di default

export "COMMAND"="echo"
export "TRIGGER"="trigger"
export "OUTPUT"="output"
export "SERVER"="127.0.0.1"
export "PORT"="1883"

cargo run


//Per vederne il funzionamento avviare anche un subscribe con trigger OUTPUT

mosquitto_sub -h 127.0.0.1 -p 1883 -t output

//e un publisher per inviare al topic TRIGGER qualcosa

mosquitto_pub -h 127.0.0.1 -p 1883 -t trigger -m messaggio
