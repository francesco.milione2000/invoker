extern crate paho_mqtt as mqtt;

use std::env;
use std::process;
use std::time::SystemTime;
use std::process::Command;


fn main(){
    //Variabili d'ambiente
    let command = env::var("COMMAND").unwrap_or("echo ciao".to_string());
    let sub_topic = env::var("TRIGGER").unwrap_or("trigger".to_string());
    let output_topic = env::var("OUTPUT").unwrap_or("output".to_string());
    let server = env::var("SERVER").unwrap_or("127.0.0.1".to_string());
    let port = env::var("PORT").unwrap_or("1883".to_string());

    println!(
        "Inizio ascolto su: {}, porta: {}, topic:{}, output:{}, command:{}",
        server, port, sub_topic, output_topic, command
    );

    let mut address = String::new();
    address.push_str("tcp://");
    address.push_str(&server);
    address.push_str(":");
    address.push_str(&port);

    //Inizializzo client
    let mut cli = mqtt::Client::new(address.clone()).unwrap_or_else(|err| {
        println!("Errore creazione Client sub: {}", err);
        process::exit(1);
    });
    let cli_pub = mqtt::AsyncClient::new(address.clone()).unwrap_or_else(|err| {
        println!("Errore creazione Client pub: {}", err);
        process::exit(1);
    });
    let conn_opts = mqtt::ConnectOptions::new();

    // Inizializzo il consumer
    let rx = cli.start_consuming();

    //Mi connetto
    if let Err(e) = cli.connect(conn_opts.clone()) {
        println!("Errore connessione:{:?}", e);
        process::exit(1);
    }
    if let Err(e) = cli_pub.connect(conn_opts.clone()).wait() {
        println!("Errore connessione: {:?}", e);
        process::exit(1);
    }

    //Mi Sottosctivo
    if let Err(e) = cli.subscribe(&sub_topic, 0 as i32) {
        println!("Errore iscrizione al topic: {:?}", e);
        process::exit(1);
    }

    //Ciclo per messaggi ricevuti
    for msg in rx.iter() {
        if let Some(msg) = msg {
            println!("Ricevo: {}", msg);
            let parsed = String::from_utf8_lossy(&msg.payload());
            let cmd = format!("{} '{}'", command, parsed);
            println!("Comando ricevuto: {}", cmd);
            let sys_time = SystemTime::now();

            let output = Command::new("/bin/bash")
            .arg("-c")
            .arg(&cmd)
            .output()
            .expect("failed to execute process");

            let stdout = String::from_utf8_lossy(&output.stdout).to_string();
            let lstdout = stdout.clone();
            let new_sys_time = SystemTime::now();

            if output.status.code().unwrap() != 8 {
                println!("Mando messaggio di output");

                let msg = mqtt::Message::new(output_topic.clone(), stdout.clone(), 0);
                if let Err(e) = cli.publish(msg){
                    println!("Errore invio:{:?}", e);
                    process::exit(1);
                }
                
            };

            println!("stdout: {}", lstdout);
            println!("status: {}", output.status);
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));

            let difference = new_sys_time
            .duration_since(sys_time)
            .expect("Clock may have gone backwards");
            println!("{:?}", difference);

        }
        else if !cli.is_connected() {
            println!("Disconnessione non voluta");
            process::exit(1);
        }
    }

    // Mi disconnetto
    if cli.is_connected() {
        println!("Disconnessione");
        cli.unsubscribe(&sub_topic).unwrap();
        cli.disconnect(None).unwrap();
    }
    println!("Fine");

}
